# README


Docker

```
  sudo docker run -d \
  --name=postgres \
  -v /etc/localtime:/etc/localtime:ro \
  -e POSTGRES_USER=root \
  -e POSTGRES_PASSWORD=root \
  -v /storage/pgdata:/var/lib/postgresql/data \
  -p 5432:5432 \
  --restart=always \
  postgres
```

Código indicadores de base

```
<div class="col">
  <div class="form-group">
    <%= form.label :origem %></br>
    <div class="custom-control custom-radio custom-control-inline">
      <%= form.radio_button :origem, :pje, class: 'custom-control-input',
          data: { 'show-attributes': 'consulta'} %>
      <%= form.label :origem_pje, 'PJe', class: 'custom-control-label' %>
    </div>
    <div class="custom-control custom-radio custom-control-inline">
      <%= form.radio_button :origem, :sadp, class: 'custom-control-input',
          data: { 'hide-attributes': 'consulta'} %>
      <%= form.label :origem_sadp, 'SADP', class: 'custom-control-label' %>
    </div>
  </div>
</div>

```

```
<div class="form-group">
  <%= f.label :tipo %>
  <%= f.collection_select :tipo_eq,
      Variavel.tipos.map { |key, value| [value, Variavel.human_enum_name(:tipos, key)]},
      :first, :last, { include_blank: 'Todos' },
      { class: 'form-control' } %>
</div>
```
