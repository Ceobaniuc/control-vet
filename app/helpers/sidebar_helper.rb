module SidebarHelper
  def nav_dono
    [
      {
        url: donos_path,
        title: 'Todos os donos'
      },
      {
        url: new_dono_path,
        title: 'Adicionar dono'
      }
    ]
  end

  def nav_animal
      [
        {
          url: animais_path,
          title: 'Todos os animais'
        },
        {
          url: new_animal_path,
          title: 'Adicionar Animal'
        }
      ]
      #TODO TERMINAR O NAVBAR
    end

  #Navegação do dono
  def nav_helper_dono style, tag_type
    nav_links =  ''

    nav_dono.each do |item|
      nav_links << "<#{tag_type}><a href='#{item[:url]}' class='#{style} #{active? item[:url]}'>#{item[:title]}</a></#{tag_type}>"
    end

    nav_links.html_safe
  end


  #Navegação do animal
  def nav_helper_animal style, tag_type
    nav_links =  ''

    nav_animal.each do |item|
      nav_links << "<#{tag_type}><a href='#{item[:url]}' class='#{style} #{active? item[:url]}'>#{item[:title]}</a></#{tag_type}>"
    end

    nav_links.html_safe
  end


  def active? path
    "active" if current_page? path
  end
end
