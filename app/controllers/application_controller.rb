class ApplicationController < ActionController::Base

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to '/', :alert => exception.message
  end

  protect_from_forgery with: :exception
  before_action :authenticate_user!
  include DeviseWhitelist


end
