class Animal < ApplicationRecord
  belongs_to :dono

  has_one_attached :foto

  enum status: {curado: 0, internado: 1, observacao: 2}
  attribute :status, :integer
  enum sexo: {macho: 0, femea: 1}

  validates :nome, length: { minimum: 1, maximum: 255 }
  validates :idade, numericality: { only_integer: true,
                                    less_than_or_equal_to: 999 }
  validates :doenca, length: { minimum: 1, maximum: 255 }
  validates :medicamento, length: { minimum: 1, maximum: 255 }

  ransacker :status do |parent|
    parent.table[:status]
  end
end
