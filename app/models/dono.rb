class Dono < ApplicationRecord
  has_many :animais, dependent: :restrict_with_error

  validates :nome, length: { minimum: 1, maximum: 255 }
  validates :idade, numericality: { only_integer: true,
                                    less_than_or_equal_to: 999 }
  validates :telefone, length: { minimum: 1, maximum: 255 }
  validates :cep, length: { minimum: 1, maximum: 255 }

end
