json.extract! dono, :id, :nome, :idade, :cep, :complemento, :rua, :bairro, :numero_casa, :telefone, :email, :created_at, :updated_at
json.url dono_url(dono, format: :json)
