json.extract! animal, :id, :nome, :idade, :sexo, :status, :foto, :doenca, :medicamento, :horario_medicamento_incio, :horario_medicamento_fim, :dono_id, :created_at, :updated_at
json.url animal_url(animal, format: :json)
