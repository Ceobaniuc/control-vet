Rails.application.routes.draw do
  get 'dashboard/dashboard', to: 'dashboard#dashboard', as: 'dashboard'
  devise_for :user, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }
  resources :animais
  resources :donos

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  # devise_scope :user do
  #   root 'devise/sessions#new'
  # end

  root to: 'dashboard#dashboard'
end
