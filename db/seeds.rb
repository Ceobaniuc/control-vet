# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Donos
  Dono.create nome: "Pedro", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "pedro@email"
  Dono.create nome: "Joao", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "joao@email"
  Dono.create nome: "Thiago", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "thiago@email"
  Dono.create nome: "Carlos", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "carlos@email"
  Dono.create nome: "Guilherme", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "guilherme@email"
  Dono.create nome: "Antonia", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "antonia@email"
  Dono.create nome: "José", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "jose@email"
  Dono.create nome: "Amanda", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "amanda@email"
  Dono.create nome: "Joana", idade: "20", cep: "12345678", complemento: "casa", rua: "rua example", bairro: "Bairro example", numero_casa: "1234", telefone: "(69)12345-6789", email: "joana@email"



#Animais
  Animal.create nome: "Rex", idade: "5", sexo: :macho, raca: "indefinida", status: :curado, doenca: "teste", medicamento: "remedio", dono_id: 1
  Animal.create nome: "Pepito", idade: "5", sexo: :femea, raca: "indefinida", status: :internado, doenca: "teste", medicamento: "remedio", dono_id: 2
  Animal.create nome: "Peck", idade: "5", sexo: :macho, raca: "indefinida", status: :observacao, doenca: "teste", medicamento: "remedio", dono_id: 3
  Animal.create nome: "Chuck", idade: "5", sexo: :femea, raca: "indefinida", status: :curado, doenca: "teste", medicamento: "remedio", dono_id: 4
  Animal.create nome: "Migle", idade: "5", sexo: :macho, raca: "indefinida", status: :internado, doenca: "teste", medicamento: "remedio", dono_id: 5
  Animal.create nome: "Sasha", idade: "5", sexo: :femea, raca: "indefinida", status: :observacao, doenca: "teste", medicamento: "remedio", dono_id: 6
  Animal.create nome: "Laila", idade: "5", sexo: :macho, raca: "indefinida", status: :curado, doenca: "teste", medicamento: "remedio", dono_id: 7
  Animal.create nome: "Laika", idade: "5", sexo: :femea, raca: "indefinida", status: :internado, doenca: "teste", medicamento: "remedio", dono_id: 8
  Animal.create nome: "Chica", idade: "5", sexo: :macho, raca: "indefinida", status: :observacao, doenca: "teste", medicamento: "remedio", dono_id: 9
