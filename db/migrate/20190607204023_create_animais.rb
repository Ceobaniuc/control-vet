class CreateAnimais < ActiveRecord::Migration[5.2]
  def change
    create_table :animais do |t|
      t.string :nome
      t.integer :idade
      t.integer :sexo
      t.string :raca
      t.integer :status
      t.text :foto
      t.string :doenca
      t.string :medicamento
      t.time :horario_medicamento_incio
      t.time :horario_medicamento_fim
      t.references :dono, foreign_key: true

      t.timestamps
    end
  end
end
