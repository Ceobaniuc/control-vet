class CreateDonos < ActiveRecord::Migration[5.2]
  def change
    create_table :donos do |t|
      t.string :nome
      t.integer :idade
      t.string :cep
      t.string :complemento
      t.string :rua
      t.string :bairro
      t.integer :numero_casa
      t.string :telefone
      t.string :email

      t.timestamps
    end
  end
end
