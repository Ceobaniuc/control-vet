require "application_system_test_case"

class AnimaisTest < ApplicationSystemTestCase
  setup do
    @animal = animais(:one)
  end

  test "visiting the index" do
    visit animais_url
    assert_selector "h1", text: "Animais"
  end

  test "creating a Animal" do
    visit animais_url
    click_on "New Animal"

    fill_in "Doenca", with: @animal.doenca
    fill_in "Dono", with: @animal.dono_id
    fill_in "Foto", with: @animal.foto
    fill_in "Horario medicamento fim", with: @animal.horario_medicamento_fim
    fill_in "Horario medicamento incio", with: @animal.horario_medicamento_incio
    fill_in "Idade", with: @animal.idade
    fill_in "Medicamento", with: @animal.medicamento
    fill_in "Nome", with: @animal.nome
    fill_in "Sexo", with: @animal.sexo
    check "Status" if @animal.status
    click_on "Create Animal"

    assert_text "Animal was successfully created"
    click_on "Back"
  end

  test "updating a Animal" do
    visit animais_url
    click_on "Edit", match: :first

    fill_in "Doenca", with: @animal.doenca
    fill_in "Dono", with: @animal.dono_id
    fill_in "Foto", with: @animal.foto
    fill_in "Horario medicamento fim", with: @animal.horario_medicamento_fim
    fill_in "Horario medicamento incio", with: @animal.horario_medicamento_incio
    fill_in "Idade", with: @animal.idade
    fill_in "Medicamento", with: @animal.medicamento
    fill_in "Nome", with: @animal.nome
    fill_in "Sexo", with: @animal.sexo
    check "Status" if @animal.status
    click_on "Update Animal"

    assert_text "Animal was successfully updated"
    click_on "Back"
  end

  test "destroying a Animal" do
    visit animais_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Animal was successfully destroyed"
  end
end
