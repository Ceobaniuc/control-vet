require 'test_helper'

class AnimaisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @animal = animais(:one)
  end

  test "should get index" do
    get animais_url
    assert_response :success
  end

  test "should get new" do
    get new_animal_url
    assert_response :success
  end

  test "should create animal" do
    assert_difference('Animal.count') do
      post animais_url, params: { animal: { doenca: @animal.doenca, dono_id: @animal.dono_id, foto: @animal.foto, horario_medicamento_fim: @animal.horario_medicamento_fim, horario_medicamento_incio: @animal.horario_medicamento_incio, idade: @animal.idade, medicamento: @animal.medicamento, nome: @animal.nome, sexo: @animal.sexo, status: @animal.status } }
    end

    assert_redirected_to animal_url(Animal.last)
  end

  test "should show animal" do
    get animal_url(@animal)
    assert_response :success
  end

  test "should get edit" do
    get edit_animal_url(@animal)
    assert_response :success
  end

  test "should update animal" do
    patch animal_url(@animal), params: { animal: { doenca: @animal.doenca, dono_id: @animal.dono_id, foto: @animal.foto, horario_medicamento_fim: @animal.horario_medicamento_fim, horario_medicamento_incio: @animal.horario_medicamento_incio, idade: @animal.idade, medicamento: @animal.medicamento, nome: @animal.nome, sexo: @animal.sexo, status: @animal.status } }
    assert_redirected_to animal_url(@animal)
  end

  test "should destroy animal" do
    assert_difference('Animal.count', -1) do
      delete animal_url(@animal)
    end

    assert_redirected_to animais_url
  end
end
