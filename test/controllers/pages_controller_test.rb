require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get observacao" do
    get pages_observacao_url
    assert_response :success
  end

  test "should get internado" do
    get pages_internado_url
    assert_response :success
  end

  test "should get curado" do
    get pages_curado_url
    assert_response :success
  end

end
